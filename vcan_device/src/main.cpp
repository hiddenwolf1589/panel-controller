/** \file main.cpp
 *
 * \brief Test thread class definition
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#include <unistd.h>
#include <iostream>
#include <cstdint>
#include <string>
#include <functional>

#include "CanSocket.h"
#include "UserInput.h"
#include "CanThread.h"

const std::string iface = "can0";

#include <iomanip>
/// Convert binary data into hex strings
/// \param structPtr - pointer to data to output
/// \param size - amount of data to convert
/// \return - converted string
std::string convertToString(void *structPtr, unsigned int size) {
    std::stringstream output;

    // Cast data to char string for easier access and iteration
    unsigned char *outputData = (unsigned char *)structPtr;

    // Setup stream for output
    output << std::hex << std::setfill('0');

    // Iterate through object
    for (int currPos = 0; currPos < size; currPos++) {
        // If not the first row and a multiple of 16 add a new line
        if (currPos != 0 && currPos % 16 == 0) {
            output << std::endl;
        }

        // Output formatted data
        output << "0x" << std::setw(2) << (int)outputData[currPos] << " ";
    }

    return output.str();
}

int main() {
    int32_t returnVal = 0;
    UserInput handler;
    CanThread cThread(iface);

    // Setup user interface functions to the can thread
    std::function<void(void)> threadStart = [ObjectPtr = &cThread] { ObjectPtr->Start(); std::cout << "Thread started: " << ObjectPtr->IsRunning() << std::endl; };
    std::function<void(void)> threadStop = [ObjectPtr = &cThread] { ObjectPtr->Stop(); std::cout << "Thread stopped: " << ObjectPtr->IsStopped() << std::endl; };
    std::function<void(void)> threadCount = [ObjectPtr = &cThread] { std::cout << "Frame count: " << ObjectPtr->Count() << std::endl; };
    std::function<void(void)> threadRead = [ObjectPtr = &cThread] { canfd_frame frame; std::cout << ((ObjectPtr->GetFrame(frame) == 0) ? convertToString((void *)&frame, sizeof(frame)) : "No frames to read") << std::endl << std::flush; };

    // Add interface classes to UI command list
    handler.AddInputFunc("start", "Start test thread", threadStart);
    handler.AddInputFunc("stop", "Stop test thread", threadStop);
    handler.AddInputFunc("count", "Get can tread frame count", threadCount);
    handler.AddInputFunc("read", "Read can thread frame", threadRead);

    // Setup and add can filter
    can_filter filter = {.can_id = 0x100, .can_mask = 0x700};
    int rtn = cThread.AddFilter(filter);

    // Start accepting can messages
    cThread.Start();

    // Verify can thread started successfully
    bool canStarted = cThread.IsRunning();
    if (canStarted == true) {
        // Loop through user input until they decide to quit
        while (handler.RouteCommand()) {
            usleep(100);
        }
    }
    else {
        std::cout << "Unable to connect to can bus.  Exiting...\n";
        returnVal = 1;
    }

    cThread.Stop();
    return returnVal;
}