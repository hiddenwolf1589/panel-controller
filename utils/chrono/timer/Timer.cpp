/** \file Timer.h
 *
 * \brief Timer class implementation
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#include "Timer.h"

Timer::Timer() :
        mStartTime(std::chrono::microseconds{0}) {
    mIsStarted = false;
}

Timer::~Timer() = default;

void Timer::Start() {
    // Get current time and set started flag to true
    mStartTime = std::chrono::steady_clock::now();
    mIsStarted = true;
}

std::chrono::microseconds Timer::Duration() {
    // Setup return variable and give default value
    std::chrono::microseconds uSec(0);

    // If started get current time and find the difference, assigning the difference to the return variable
    if (mIsStarted) {
        std::chrono::steady_clock::time_point currTime = std::chrono::steady_clock::now();
        uSec = std::chrono::duration_cast<std::chrono::microseconds>(currTime - mStartTime);
    }

    return uSec;
}