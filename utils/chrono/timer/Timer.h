/** \file Timer.h
 *
 * \brief Timer class definition
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#ifndef DEV_MAIN_TIMER_H
#define DEV_MAIN_TIMER_H

#include <chrono>

class Timer {
public:
    Timer();
    ~Timer();

    ///  Start the timer
    void Start();

    /// Get the duration for the current timer
    /// \return - time since start, defaults to 0 if not started
    std::chrono::microseconds Duration();

protected:
    bool mIsStarted;
    std::chrono::steady_clock::time_point mStartTime;

private:

};


#endif //DEV_MAIN_TIMER_H
