/** \file SocketBase.h
 *
 * \brief Socket base class definition
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#ifndef DEV_MAIN_SOCKETBASE_H
#define DEV_MAIN_SOCKETBASE_H

#include <string>
#include <cstring>

class SocketBase {
public:
    /// Class constructor - no socket name
    SocketBase();

    /// Class constructor - named socket
    /// \param typeStr - name for socket
    explicit SocketBase(std::string typeStr);

    /// Virtual socket destructor
    virtual ~SocketBase();

    /// Basic socket connection
    /// \return - connection result status.  See ACTION_* static const variables below
    int Connect();

    /// Basic socket disconnect
    /// \return - connection result status.  See ACTION_* static const variables below
    int Disconnect();

    /// Basic socket reconnection
    /// \return - connection result status.  See ACTION_* static const variables below
    int Reconnect();

    /// Virtual function to read basic string from the socket
    /// \param str - reference to string to be set
    /// \return - length of string read from socket
    virtual size_t Read(std::string &str) = 0;

    /// Virtual function to write basic string to the socket
    /// \param str - reference to string to be set
    /// \return - length of string written to socket
    virtual size_t Write(std::string &str) = 0;

    /// Virtual function to read raw data from the socket
    /// \param dataPtr - pointer to data variable
    /// \param size - max amount of data to read from the socket
    /// \return - length of data read from socket
    virtual size_t Read(void *dataPtr, size_t size) = 0;

    /// Virtual function to write raw data to the socket
    /// \param dataPtr - pointer to data variable
    /// \param size - max amount of data to written to the socket
    /// \return - length of data written to socket
    virtual size_t Write(void *dataPtr, size_t size) = 0;

    /// Virtual function for checking socket connection status
    /// \return - boolean connection status, true - connected / false - disconnected
    virtual bool IsConnected() = 0;

    /// Static result values
    static const int ACTION_SUCCESS = 0;
    static const int ACTION_NEUTRAL = 1;
    static const int ACTION_FAIL = -1;
protected:

    // Internal variable
    const std::string mTypeStr;
    bool mPropsChanded;

    /// Virtual function for socket specific setup
    virtual void SetupSocket() = 0;

    /// Virtual function for platform specific error reporting
    /// \param msg - message to output
    virtual void PlatformError(std::string msg) = 0;

    /// Virtual function for platform specific connection, used in the base socket connect function calls
    /// \return - connection result status.  See ACTION_* static const variables above
    virtual int PlatformConnect() = 0;

    /// Virtual function for platform specific disconnection, used in the base socket disconnect function calls
    /// \return - connection result status.  See ACTION_* static const variables above
    virtual int PlatformDisconnect() = 0;

    /// Class function for outputting errors
    /// \param msg - error message to output
    void OutputError(std::string msg);

    /// Check for property changes and reconnect if needed
    void CheckPropChanges();

private:
    /// Class initialization function.  Used to simplify multiple constructors
    void Init();
};

#endif //DEV_MAIN_SOCKETBASE_H