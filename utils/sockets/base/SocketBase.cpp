/** \file SocketBase.cpp
 *
 * \brief Socket base class implementation
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#include "SocketBase.h"

SocketBase::SocketBase() : mTypeStr("undefined socket type") {
    // Init the socket even though no information was provided
    Init();
}

SocketBase::SocketBase(std::string typeStr) : mTypeStr(typeStr) {
    // Setup the socket
    Init();
}

SocketBase::~SocketBase() {
    // Don't do any disconnecting on deconstruction, derived class should handle that.
}

int SocketBase::Connect() {
    int rtn = ACTION_FAIL;

    // Check to see if properties have changed.  If they have set up the socket again
    if (mPropsChanded) {
        SetupSocket();
    }

    rtn = PlatformConnect();

    return rtn;
}

int SocketBase::Disconnect() {
    int rtn = ACTION_SUCCESS;
    // If the socket isn't 0, then we're connected
    if (IsConnected()) {
        // Close the socket
        rtn = PlatformDisconnect();
    }
    return rtn;
}

int SocketBase::Reconnect() {
    int rtn = ACTION_SUCCESS;

    // Check to see if the socket is connected
    if (IsConnected()) {
        // If it is, disconnect so a reconnect can occur
        rtn = Disconnect();
    }

    // If there have been no errors try to reconnect
    if (rtn == ACTION_SUCCESS) {
        rtn = Connect();
    }

    return rtn;
}

void SocketBase::OutputError(std::string msg) {
    // Prep output string
    std::string errorMsg = mTypeStr + " - "  + msg;

    // output error
    PlatformError(errorMsg);
}

void SocketBase::Init() {
    mPropsChanded = false;
}

void SocketBase::CheckPropChanges() {
    // Check to see if there has been changes to properties and whether the socket is connected
    if (mPropsChanded && IsConnected()) {
        // We need to reset the connection
        Reconnect();
    }
}