/** \file CanSocket.h
 *
 * \brief CAN socket class definition
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#ifndef DEV_MAIN_CANSOCKET_H
#define DEV_MAIN_CANSOCKET_H

#include <iostream>

#include <sys/ioctl.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#ifdef __linux

#include "LinuxSocket.h"
typedef LinuxSocket PlatformSocket;

#endif

class CanSocket : public PlatformSocket {
public:
    /// Can socket contructor
    /// \param iface - interface to bind to
    explicit CanSocket(const std::string &iface);

    /// Can socket destructor
    ~CanSocket() override;

    /// Can socket protocol to use.  RAW is the only one inplemented at the moment 07/22/2020
    enum PROTOCAL {
        INVALID = 0,
        RAW = CAN_RAW,
        BCM = CAN_BCM,
        NUM_PROTOCALS
    };

    /// Function to change protocol if needed
    /// \param proto - protocol via PROTOCOL enum
    void SetProtocol(PROTOCAL proto);

    /// \todo Add a way to enable/disable fd frames

    /// Read canfd frame from interface
    /// \todo add a way to provide fd or regular frame
    /// \param frame - reference to can frame to place data in
    /// \return - read status, see SocketBase for status'
    int Read(canfd_frame &frame);

    /// Write can frame to interface (untested)
    /// \todo add a way to provide fd or regular frame
    /// \param frame - reference to can frame write
    /// \return - read status, see SocketBase for status'
    int Write(can_frame &frame);

    /// Add filter to interface to receive frames
    /// \todo add abstraction layer for filter input to be allow general platform use
    /// \todo generalize return values
    /// \param filter - linux interface
    /// \return - platform specific filter add result
    int AddFilter(can_filter &filter);

protected:
    static constexpr int FRAME_SIZE = sizeof(canfd_frame);

    // internal class variables
    PROTOCAL mCanProtocol;
    struct sockaddr_can mSockAddr;
    ifreq mIfr;

    /// Base class socket setup override
    void SetupSocket() override;

private:
    // Internal storage of interface name
    const std::string mInterfaceName;

};

#endif //DEV_MAIN_CANSOCKET_H