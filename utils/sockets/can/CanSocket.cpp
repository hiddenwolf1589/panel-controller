/** \file CanSocket.cpp
 *
 * \brief CAN socket class implementation
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#include "CanSocket.h"

CanSocket::CanSocket(const std::string &iface) :
        PlatformSocket("can"),
        mCanProtocol(INVALID),
        mInterfaceName(iface) {
    SetupSocket();
}

CanSocket::~CanSocket() = default;

void CanSocket::SetProtocol(PROTOCAL proto) {
    // Set internal protocol variable if valid
    if (proto >= RAW && proto < NUM_PROTOCALS) {
        mCanProtocol = proto;
        mPropsChanded = true;
    }
}

int CanSocket::Read(canfd_frame &frame) {
    // Platform specific read
    int readSize = PlatformSocket::Read(&frame, FRAME_SIZE);

    // Determine the result value based on the read size
    int rtnVal;
    switch (readSize) {
        case 0:
            rtnVal = ACTION_NEUTRAL;
            break;
        case FRAME_SIZE:
            rtnVal = ACTION_SUCCESS;
            break;
        default:
            rtnVal = ACTION_FAIL;
    }
    return rtnVal;
}

int CanSocket::Write(can_frame &frame) {
    return (PlatformSocket::Write(&frame, FRAME_SIZE) == FRAME_SIZE) ? ACTION_SUCCESS : ACTION_FAIL;
}

int CanSocket::AddFilter(can_filter &filter) {
#ifdef __linux
    return setsockopt(mSocketFd, SOL_CAN_RAW, CAN_RAW_FILTER, &filter, sizeof(filter));
#else
    return -1;
#endif
}

// Platform specific socket setup
#ifdef __linux

void CanSocket::SetupSocket() {
    bool error = false;

    // Setup socket
    mSocketFd = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    std::cout << "Creating " << mTypeStr << " socket...\n";
    if (mSocketFd < 0) {
        OutputError("socket");
        error = true;
    }
    else {
        std::cout << mTypeStr << " socket created\n";
    }

    // if socket setup was successful get interface information from system
    if (error == false) {
        std::cout << "Identifying \"" << mInterfaceName << "\" socket...\n";
        memset(&mIfr, 0, sizeof(mIfr));
        strncpy(mIfr.ifr_name, mInterfaceName.c_str(), mInterfaceName.length());
        int32_t rtn = ioctl(mSocketFd, SIOCGIFINDEX, &mIfr);
        if (rtn < 0) {
            OutputError("ioctl");
            error = true;
        }
        else {
            std::cout << "Identified \"" << mInterfaceName << "\" interface!\n";
        }
    }

    // If interface was identified setup the base class to bind the socket and set any desired socket options
    if (error == false) {
        std::cout << "Set up socket address.\n";
        sockaddr_can *sockAddrPtr = &mSockAddr;
        int sockAddrSize = sizeof(mSockAddr);
        memset(sockAddrPtr, 0, sockAddrSize);
        mSockAddr.can_family = AF_CAN;
        mSockAddr.can_ifindex = mIfr.ifr_ifindex;

        mSockAddrPtr = reinterpret_cast<sockaddr *>(sockAddrPtr);
        mSockAddrSize = sockAddrSize;

        // Setup to allow FD frames
        int enable = 1;
        int rtn = setsockopt(mSocketFd, SOL_CAN_RAW, CAN_RAW_FD_FRAMES, &enable, sizeof(enable));
        if (rtn != 0) {
            OutputError("fd_frame setsockopt");
        }

        mPropsChanded = false;
    }
}

#else

void CanSocket::SetupSocket() {
    // TODO - Fill in other platform socket init as needed
}

#endif