/** \file LinuxSocket.h
 *
 * \brief Linux socket class definition
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#ifndef DEV_MAIN_LINUXSOCKET_H
#define DEV_MAIN_LINUXSOCKET_H

#include "SocketBase.h"

#include <unistd.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>

class LinuxSocket : public SocketBase {
public:
    /// Linux socket constructor - no type string
    LinuxSocket();

    /// Linux socket constructor - type string provided
    explicit LinuxSocket(std::string typeStr);

    /// Class destructor
    ~LinuxSocket();

    /// Platform specific virtual string read - base class override
    /// \param str - reference to be used for string placement
    /// \return - string read size
    virtual size_t Read(std::string &str) override;

    /// Platform specific virtual string write - base class override
    /// \param str - reference to be used writing data to socket
    /// \return - string write size
    virtual size_t Write(std::string &str) override;

    /// Platform specific virtual raw data read - base class override
    /// \param dataPtr - pointer to data to write
    /// \param size - size of data to write
    /// \return - bytes read from socket
    virtual size_t Read(void *dataPtr, size_t size) override;

    /// Platform specific virtual raw data write - base class override
    /// \param dataPtr - pointer to data to write
    /// \param size - size of data to write
    /// \return - bytes written to socket
    virtual size_t Write(void *dataPtr, size_t size) override;

    /// Platform specific function for writing - base class override
    bool IsConnected() override;
protected:

    /// Platform specific connect - base class override
    /// \return - connection result status.  See ACTION_* static const variables in SocketBase
    int PlatformConnect() override;

    /// Platform specific disconnect - base class override
    /// \return - disconnection result status.  See ACTION_* static const variables in SocketBase
    int PlatformDisconnect() override;

    /// Platform specific error - base class override
    /// \param msg - error message to output
    void PlatformError(std::string msg) override;

    // internal variables
    int mSocketFd;
    sockaddr *mSockAddrPtr;
    int mSockAddrSize;

    fd_set mReadSet;
    timeval mReadTimeout = {.tv_sec =  0, .tv_usec = 10000};

private:
    /// Platform specific socket init
    void Init();

    /// Platform non-virtual disconnect
    int disconnect();
};


#endif //DEV_MAIN_LINUXSOCKET_H
