/** \file LinuxSocket.cpp
 *
 * \brief Linux socket class implementation
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#include "LinuxSocket.h"

LinuxSocket::LinuxSocket() : SocketBase() {
    Init();
}

LinuxSocket::LinuxSocket(std::string typeStr) : SocketBase(typeStr) {
    Init();
}

LinuxSocket::~LinuxSocket() {
    disconnect();
}

size_t LinuxSocket::Read(std::string &str) {
    const int BUFF_SIZE = 1024;
    int readCount = 0;
    int totalCount = 0;
    char buff[BUFF_SIZE];
    char copyBuff[BUFF_SIZE + 1];

    // Check for property changes
    CheckPropChanges();

    // Setup output string
    str.clear();

    // Read from the socket until the end of the file
    do {
        readCount = read(mSocketFd, buff, BUFF_SIZE);

        // If readCount is positive then there was data read, handle it
        if (readCount > 0) {
            memset(&copyBuff, 0, BUFF_SIZE);
            memcpy(&copyBuff, &buff, readCount);
            str += copyBuff;
            totalCount += readCount;
        }
            // If readCount is negative then there was an error
        else if (readCount < 0) {
            OutputError("read_str");
            Disconnect();
        }
        // Otherwise readCount was 0 and there is no more data to read
    } while (readCount > 0);

    // If there was an error output the error value, otherwise output the total read count
    return (readCount < 0) ? readCount : totalCount;
}

size_t LinuxSocket::Write(std::string &str) {
    // Check for property changes
    CheckPropChanges();

    // Write to the socket
    return write(mSocketFd, str.c_str(), str.length());
}

size_t LinuxSocket::Read(void *dataPtr, size_t size) {
    const int BUFF_SIZE = 1024;
    bool error = false;
    int readCount = 0;
    int totalCount = 0;
    char buff[BUFF_SIZE];
    char tempBuff[size];

    // Check for property changes
    CheckPropChanges();

    // Prep read set before select statement
    FD_ZERO(&mReadSet);
    FD_SET(mSocketFd, &mReadSet);

    int rtn = select(mSocketFd + 1, &mReadSet, nullptr, nullptr, &mReadTimeout);
    if (rtn > 0) {
        // Read from the socket until the end of the file
        do {
            readCount = read(mSocketFd, buff, BUFF_SIZE);

            // If readCount is positive then there was data read, handle it
            if (readCount > 0) {
                // Check to make sure there is still room in the buffer
                if ((readCount + totalCount) < size) {
                    // There wasn't enough room, indicate error
                    readCount = -1;
                    error = true;
                } else {
                    // If there is, transfer to the read buffer and increase the total count
                    memcpy(tempBuff + totalCount, &buff, readCount);
                    totalCount += readCount;
                }
            }
                // If readCount is negative then there was an error
            else if (readCount < 0) {
                OutputError("read_raw");
                Disconnect();
                error = true;
            }
        } while (readCount == BUFF_SIZE);

        // There wasn't an error, so copy the read data to the output buffer
        if (error == false) {
            memcpy(dataPtr, tempBuff, totalCount);
        }
    }
    else if (rtn < 0) {
        OutputError("select");
    }

    // If there was an error output the error value, otherwise output the total read count
    return (readCount < 0) ? readCount : totalCount;
}

size_t LinuxSocket::Write(void *dataPtr, size_t size) {
    // Check for property changes
    CheckPropChanges();

    // Write to the socket
    return write(mSocketFd, dataPtr, size);
}

bool LinuxSocket::IsConnected() {
    // If the socket is not 0 then we're connected
    return mSocketFd != 0;
}

int LinuxSocket::PlatformConnect() {
    int rtn = ACTION_FAIL;
    // Check to make sure everything went as expected during setup
    if (mSocketFd != 0 && mSockAddrPtr != nullptr) {
        // Bind to the socket and check for errors
        rtn = bind(mSocketFd, mSockAddrPtr, mSockAddrSize);
        if (rtn < 0) {
            OutputError("bind");
        }
    }
    else {
        OutputError("socket setup");
    }

    return rtn;
}

int LinuxSocket::PlatformDisconnect() {
    // Class deconstructer wants access to disconnect functionality so a separate function was created and called here
    return Disconnect();
}

void LinuxSocket::PlatformError(std::string msg) {
    // output error
    perror(msg.c_str());
}

void LinuxSocket::Init() {
    mSocketFd = 0;
    mSockAddrSize = 0;
    mSockAddrPtr = nullptr;
    mPropsChanded = false;
    FD_ZERO(&mReadSet);
}

int LinuxSocket::disconnect() {
    int rtn = ACTION_SUCCESS;
    // If the socket is connected
    if (IsConnected()) {
        // Close the socket
        rtn = close(mSocketFd);
    }
    return rtn;
}