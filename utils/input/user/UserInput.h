/** \file UserInput.h
 *
 * \brief User input class definition
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#ifndef DEV_MAIN_USERINPUT_H
#define DEV_MAIN_USERINPUT_H

#include <string>
#include <map>
#include <functional>

class UserInput {
public:
    UserInput();
    ~UserInput();

    /// Add input function to the command selector
    /// \param cmd - string user uses to select functionality
    /// \param description - description to use when displaying the command in help
    /// \param func - callback function to call when user selects command
    /// \return - error indicator, false - no error / true - error occurred
    bool AddInputFunc(const std::string &cmd, const std::string &description, const std::function<void(void)> &func);

    /// Routes user input to a command and initiates the callback function
    /// \return - continue indicator, false - user has decided to exit / true - continue executing commands
    bool RouteCommand();
protected:

    /// Outputs commands and descriptions to the user
    void Help();

    /// Updates the user internal flag to indicate exit, used when looping though user input
    void Quit();

private:
    // struct for managing commands and callback functions
    struct cmd_info {
        std::string desc;
        std::function<void(void)> func;
    };

    // Type definitions for easier use later
    typedef std::pair<const std::string, cmd_info> FUNC_INFO;
    typedef std::map<std::string, cmd_info> FUNC_MAP;

    // internal variables
    const int CMD_MAX_LENGTH;
    bool mContinue;
    bool mFirstPass;
    FUNC_MAP mFunctionMap;
};


#endif //DEV_MAIN_USERINPUT_H
