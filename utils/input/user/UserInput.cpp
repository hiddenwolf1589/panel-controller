/** \file UserInput.cpp
 *
 * \brief User input class implementation
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#include "UserInput.h"

#include <iostream>
#include <iomanip>
#include <functional>

UserInput::UserInput() :
    CMD_MAX_LENGTH(8) {
    mContinue = true;
    mFirstPass = true;

    // Add internal functions to command list
    AddInputFunc("h", "lists available commands", [this]() { Help(); });
    AddInputFunc("q", "return from this menu", [this]() { Quit(); });
}

UserInput::~UserInput() = default;

bool UserInput::AddInputFunc(const std::string &cmd, const std::string &description, const std::function<void(void)> &func) {
    bool error = false;

    // Verify command isn't too long
    if (cmd.length() > CMD_MAX_LENGTH) {
        // Command code is too long
        error = true;
    }
    else {
        // Verify the command isn't already in the map
        FUNC_MAP::iterator element = mFunctionMap.find(cmd);
        if (element == mFunctionMap.end()) {
            // The command isn't mapped, add it
            mFunctionMap.emplace(FUNC_INFO(cmd, {description, func}));
        } else {
            // Function is already mapped
            error = true;
        }
    }

    return error;
}

bool UserInput::RouteCommand() {
    std::string input;

    // Check if this is the first time route has been called, if it is output the help
    if (mFirstPass) {
        Help();
        mFirstPass = false;
    }

    // Wait for the user's input
    getline(std::cin, input);
    std::cout << std::endl;

    // Get the map pair based on the command they inputted
    FUNC_MAP::iterator element = mFunctionMap.find(input);
    if (element != mFunctionMap.end()) {
        // Requested command exists so call the function
        element->second.func();
    }
    else {
        // Requested command doesn't exist
        std::cout << "Invalid command input.  Please try again...\n";
    }

    return mContinue;
}

void UserInput::Help() {
    // Initial help output
    std::cout << "Commands:\n";

    // Iterate through all mapped functions and output the command and description
    for (FUNC_INFO &extElement : mFunctionMap) {
        std::cout << "\t" << std::setw(CMD_MAX_LENGTH) << std::left << extElement.first << " -\t"
                    << extElement.second.desc << std::endl;
    }

    // Prompt the user to input a command
    std::cout << std::endl << "Please enter command to continue:\n";
}

void UserInput::Quit() {
    // Update flag
    mContinue = false;
}