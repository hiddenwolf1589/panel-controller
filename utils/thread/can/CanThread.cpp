/** \file CanThread.h
 *
 * \brief CAN thread class implementation
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#include "CanThread.h"

CanThread::CanThread(const std::string &interface) : mInterface(interface), LinuxThread(interface + "-can_thread"), CanSocket(interface) {

}

CanThread::~CanThread() = default;

int CanThread::Count() {
    std::lock_guard<std::mutex> guard = MemGuard();
    return mDataList.size();
}

int CanThread::GetFrame(canfd_frame &frame) {
    std::lock_guard<std::mutex> guard = MemGuard();
    int rtnVal = -1;
    // If data queue isn't empty get data
    if (mDataList.empty() == false) {
        // Store data locally and pop off old storage
        canfd_frame tempFrame(mDataList.front());
        mDataList.pop();

        // Copy data into place
        memcpy(&frame, &tempFrame, sizeof(tempFrame));

        // Update return value to indicate no error
        rtnVal = 0;
    }
    return rtnVal;
}

bool CanThread::LoopInit() {
    return (Connect() != -1) ? THREAD_SUCCESS : THREAD_FAILURE;
}

bool CanThread::LoopFunc() {
    canfd_frame frame;

    // Block loop while waiting for frame read
    int readStat = Read(frame);

    // If status indicates a read add data to queue
    if (readStat == CanSocket::ACTION_SUCCESS) {
        MemLock();
        mDataList.push(frame);
        MemUnlock();
    }

    // Only indicate failure if action fails.  There are two acceptable cases, success and neutral (timeout)
    return (readStat == CanSocket::ACTION_FAIL) ? THREAD_FAILURE : THREAD_SUCCESS;
}