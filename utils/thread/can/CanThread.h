/** \file CanThread.h
 *
 * \brief CAN thread class definition
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#ifndef DEV_MAIN_CANTHREAD_H
#define DEV_MAIN_CANTHREAD_H

#include "CanSocket.h"

#include <string>
#include <queue>

#ifdef __linux

#include "LinuxThread.h"
typedef LinuxThread PlatformThread;

#endif

class CanThread : public PlatformThread, public CanSocket {
public:
    CanThread(const std::string &interface);
    ~CanThread() override;

    /// Get count of waiting data packets
    /// \return - number of stored data packets
    int Count();

    /// Retrieve received frame from storage
    /// \param frame - reference to frame storage variable
    /// \return - error status, -1 - error (no frames to retrieve) / 0 - no error
    int GetFrame(canfd_frame &frame);

protected:
    /// CAN Specific thread loop init
    /// \return - thread init error status, see static const THREAD_* variables in ThreadBase
    bool LoopInit() override;

    /// CAN Specific thread loop init
    /// \return - thread loop error status, see static const THREAD_* variables in ThreadBase
    bool LoopFunc() override;

private:
    // internal variables
    const std::string mInterface;

    std::queue<canfd_frame> mDataList;
};


#endif //DEV_MAIN_CANTHREAD_H
