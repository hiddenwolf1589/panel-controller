/** \file LinuxThread.h
 *
 * \brief Linux thread class definition
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#ifndef DEV_MAIN_LINUXTHREAD_H
#define DEV_MAIN_LINUXTHREAD_H

#ifdef __linux

#include "ThreadBase.h"

#include <string>

#include <pthread.h>

class LinuxThread : public ThreadBase {
public:
    explicit LinuxThread(const std::string &name = "thread", int nice = 0, bool debug = false);
    ~LinuxThread() override;

protected:
    /// Linux platform specific thread start function
    /// \return - thread start status
    bool HandleStart() override;

    /// Linux platform specific thread stop function
    /// \return - thread stop status
    bool HandleStop() override;

    /// Linux platform specific usleep function
    /// \param usec - microsecond sleep time
    void DoUSleep(unsigned int usec) override;

private:
    /// Static function for starting pthread
    /// \param thisPtr - pointer to thread object
    static void* Runner(void *thisPtr);

    /// Linux thread variables
    const std::string mThreadName;
    pthread_t mThread;
};

#endif // __linux

#endif //DEV_MAIN_LINUXTHREAD_H
