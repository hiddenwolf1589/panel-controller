/** \file LinuxThread.h
 *
 * \brief Linux thread class implementation
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#include "LinuxThread.h"

#ifdef __linux

#include <csignal>
#include <unistd.h>

LinuxThread::LinuxThread(const std::string &name, int nice, bool debug ) :
        ThreadBase(debug),
        mThreadName(name){
    mThread = 0;
}
LinuxThread::~LinuxThread() {
    if (IsRunning()) {
        Stop();
    }
}

bool LinuxThread::HandleStart() {
    bool fncRtn = THREAD_SUCCESS;

    // If the tread is stopped lets try to start it
    if (IsStopped()) {
        // Create the pthread
        OutputDebug("Creating pthread");
        mStop = false;
        int rc = pthread_create(&mThread, nullptr, Runner, this);

        // If the thread started update the statuses and rename the thread
        if (rc == 0) {
            OutputDebug("Renaming thread");
            pthread_setname_np(mThread, mThreadName.c_str());
        }
        else {
            fncRtn = THREAD_FAILURE;
        }
    }
    return fncRtn;
}

bool LinuxThread::HandleStop() {
    bool fncRtn = THREAD_SUCCESS;

    // If the tread is running lets try to stop it
    if (IsRunning()) {
        mStop = true;

        // Try to join the thread
        OutputDebug("Trying to join thread");
        int rtn = pthread_join(mThread, nullptr);

        // If the thread joined without problem update the status
        if (rtn == 0) {
            OutputDebug("Thread joined");
        }
        // If it didn't signal to the thread that it should exit immediately
        // Update the thread to abort since we had to force it
        else {
            OutputDebug("Sending signal to kill thread");
            pthread_kill(mThread, SIGHUP);
            fncRtn = THREAD_FAILURE;
        }
    }

    return fncRtn;
}

void LinuxThread::DoUSleep(unsigned int usec) {
    // Platform specific sleep
    usleep(usec);
}

void* LinuxThread::Runner(void *thisPtr) {
    // Setup signal to handle exit if needed
    signal(SIGHUP, [](int sig) { if (sig == SIGHUP) { pthread_exit(nullptr); } } );

    LinuxThread *typedThisPtr = (LinuxThread *)thisPtr;

    // Run loop until it exits
    typedThisPtr->Process();
    pthread_exit(nullptr);
}

#endif // __linux
