/** \file TestThread.cpp
 *
 * \brief Test thread class implementation
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#include "TestThread.h"

#include <iostream>

#include "Timer.h"

TestThread::TestThread(bool debug) : LinuxThread("Test", 0, debug) {

}

TestThread::~TestThread() = default;

bool TestThread::LoopInit() {
    std::cout << "This is the tread init!\n" << std::flush;
    return THREAD_SUCCESS;
}

bool TestThread::LoopFunc() {
    std::cout << "This is the tread text!\n" << std::flush;
    return THREAD_SUCCESS;
}