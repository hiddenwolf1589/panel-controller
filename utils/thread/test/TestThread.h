/** \file TestThread.h
 *
 * \brief Test thread class definition
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#ifndef DEV_MAIN_TESTTHREAD_H
#define DEV_MAIN_TESTTHREAD_H

#include "LinuxThread.h"

class TestThread : public LinuxThread {
public:
    explicit TestThread(bool debug = false);
    ~TestThread() override;

protected:
    bool LoopInit() override;
    bool LoopFunc() override;
private:

};


#endif //DEV_MAIN_TESTTHREAD_H
