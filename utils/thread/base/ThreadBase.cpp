/** \file ThreadBase.cpp
 *
 * \brief Thread base class implementation
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#include "ThreadBase.h"

#include <iostream>

ThreadBase::ThreadBase(bool debug) :
        DEBUG(debug) {
    mStop = false;
    mInitFail = false;
    mLoopThrowCount = 0;

    mDurationSet = false;
    mThreadDuration = std::chrono::microseconds(0);

    mStatus = Status::STOPPED;
}

void ThreadBase::MemLock() {
    OutputDebug("Memory locked...");
    mDataLock.lock();
}

void ThreadBase::MemUnlock() {
    mDataLock.unlock();
    OutputDebug("Memory unlocked!");
}

std::lock_guard<std::mutex> ThreadBase::MemGuard() {
    OutputDebug("Created memory lock guard");
    return std::lock_guard<std::mutex>(mDataLock);
}

ThreadBase::Status ThreadBase::GetStatus() const {
    return mStatus;
}

bool ThreadBase::IsStopped() const {
    return mStatus == Status::STOPPED;
}

bool ThreadBase::IsRunning() const {
    return mStatus == Status::RUNNING;
}

bool ThreadBase::IsAborted() const {
    return mStatus == Status::ABORT;
}

ThreadBase::Status ThreadBase::Start() {
    OutputDebug("Thread start called");
    // Do platform specific start
    bool rtn = HandleStart();

    // Update the status based on the start result
    if (rtn == THREAD_SUCCESS) {
        mStatus = Status::RUNNING;
    } else {
        mStatus = Status::ABORT;
    }

    return mStatus;
}

ThreadBase::Status ThreadBase::Stop() {
    OutputDebug("Thread stop called");
    // Do platform specific stop
    bool rtn = HandleStop();

    // Update the status based on the stop result
    if (rtn == THREAD_SUCCESS) {
        mStatus = Status::STOPPED;
    } else {
        mStatus = Status::ABORT;
    }

    return mStatus;
}

void ThreadBase::SetLoopTime(unsigned int usec) {
    mThreadDuration = std::chrono::microseconds(usec);
    mDurationSet = usec != 0;
}

unsigned int ThreadBase::GetLoopTime() const {
    return mThreadDuration.count();
}

bool ThreadBase::isDebug() const {
    return DEBUG;
}

void ThreadBase::Process() {
    // Run through the initialization routine
    OutputDebug("Initializing thread...");
    BOOL_FUNC thisInitFunc = [this] { return LoopInit(); };
    VOID_FUNC thisInitFail = [this] { mInitFail = true; };
    TryCatchFunc(thisInitFunc, [this] { mInitFail = false; }, thisInitFail, thisInitFail);

    // If the initialization routine failed don't allow the tread to start
    if (mInitFail) {
        OutputError("Initialization failed.  Thread will not be started");
        mStop = true;
    }
        // Otherwise update the thread status
    else {
        OutputDebug("Initializing completed!");
        mStatus = Status::RUNNING;
    }

    mLoopThrowCount = 0;
    // Cause thread to start looping
    OutputDebug("Starting thread loop...");

    if (mDurationSet) {
        mThreadTimer.Start();
    }

    while (IsContinuing()) {
        if (mDurationSet == false || mThreadTimer.Duration() > mThreadDuration) {
            // Restart time
            mThreadTimer.Start();

            // Run loop function
            OutputDebug("Looping...");
            BOOL_FUNC thisLoopFunc = [this] { return LoopFunc(); };
            TryCatchFunc(thisLoopFunc, [this] { mLoopThrowCount = 0; }, [] {}, [this] { mLoopThrowCount++; });

            // If the loop function has thrown an exception 3 times in a row, shut down the thread
            if (mLoopThrowCount >= CATCH_MAX) {
                OutputDebug("Thread loop threw too errors too many times.  Aborting...");
                mStop = true;
                mStatus = Status::ABORT;
            }
        }

        DoUSleep(USLEEP_TIME);
    }

    // If the status hasn't been updated to abort change it to stopped
    if (mStatus != Status::ABORT) {
        mStatus = Status::STOPPED;
    }
    OutputDebug("Exiting thread!");
}

void ThreadBase::OutputError(const std::string &msg, bool isSysError) {
    if (isSysError) {
        std::string fmt = msg + " - ";
        perror(fmt.c_str());
    } else {
        OutputDebug(msg, true);
    }
}

void ThreadBase::OutputDebug(const std::string &msg, bool override) {
    if (isDebug() || override) {
        std::cerr << msg << std::endl << std::flush;
    }
}

bool ThreadBase::IsStopping() const {
    return mStop;
}

bool ThreadBase::IsContinuing() const {
    return !mStop;
}

void ThreadBase::TryCatchFunc(const BOOL_FUNC &func, const VOID_FUNC &success, const VOID_FUNC &fail, const VOID_FUNC &exception) {
    bool funcResult = THREAD_FAILURE;

    // Try to run the expected function
    try {
        funcResult = func();

        // If the function didn't thrown an exception, run the appropriate result function
        MemLock();
        funcResult == THREAD_SUCCESS ? success() : fail();
        MemUnlock();
    }
    catch (...) {
        // If an exception was thrown execute the fail function and update the passed flag
        MemLock();
        fail();
        MemUnlock();
    }
}