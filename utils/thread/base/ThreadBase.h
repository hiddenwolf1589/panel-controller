/** \file ThreadBase.h
 *
 * \brief Thread base class definition
 *
 * \par
 * COPYRIGHT NOTICE: (c) 2020 Dan (Gunnar) Ryder.  All rights reserved.
 */

#ifndef DEV_MAIN_THREADBASE_H
#define DEV_MAIN_THREADBASE_H

#include <mutex>
#include <functional>
#include <chrono>

#include "Timer.h"

class ThreadBase {
public:
    /// Status enum for the class
    enum class Status {
        STOPPED,
        RUNNING,
        ABORT
    };

    explicit ThreadBase(bool debug = false);
    virtual ~ThreadBase() = default;

    /// Lock memory associated with the thread
    void MemLock();

    /// Unlock memory associated with the thread
    void MemUnlock();

    /// Create lock_guard for memory using the class mutex
    std::lock_guard<std::mutex> MemGuard();

    /// Get the current status for the thread
    /// \return - enum Status of thread
    [[nodiscard]] Status GetStatus() const;

    /// Boolean stopped status indicator for the thread
    /// \return - indication whether the thread has stopped
    [[nodiscard]] bool IsStopped() const;

    /// Boolean running status indicator for the thread
    /// \return - indication whether the thread is running
    [[nodiscard]] bool IsRunning() const;

    /// Boolean abort status indicator for the thread
    /// \return - indication whether the thread has aborted
    [[nodiscard]] bool IsAborted() const;

    /// Starts the thread
    /// \return - start status
    Status Start();

    /// Stops the thread
    /// \return - stop status
    Status Stop();

    /// Sets the loop time for the thread
    /// \param usec - microsecond time between loop iterations
    void SetLoopTime(unsigned int usec);

    /// Gets the loop time for the thread
    /// \return - microsecond time between loop iterations
    [[nodiscard]] unsigned int GetLoopTime() const;

    /// Boolean debug status indicator
    /// \return - debug status
    [[nodiscard]] bool isDebug() const;

protected:
    /// Virtual loop initialization function, meant for transport specialization
    /// \return - loop init status
    virtual bool LoopInit() = 0;

    /// Virtual loop iteration function, meant for transport specialization
    /// \return - loop iteration status
    virtual bool LoopFunc() = 0;

    /// Virtual thread start function, meant for platform specialization
    /// \return - thread start status
    [[nodiscard]] virtual bool HandleStart() = 0;

    /// Virtual thread stop function, meant for platform specialization
    /// \return - thread stop status
    [[nodiscard]] virtual bool HandleStop() = 0;

    /// Thread handling after platform specific initialization
    void Process();

    /// Default loop sleep time
    const unsigned int USLEEP_TIME = 100;

    /// Virtual usleep function, meant for platform specialization
    virtual void DoUSleep(unsigned int usec) = 0;

    /// Output error message
    /// \param msg - message to output to the user
    /// \param isSysError - indication if a system error is expected
    void OutputError(const std::string &msg, bool isSysError = false);

    /// Output debug message
    /// \param msg - message to output to the user
    /// \param override - override the debug status
    void OutputDebug(const std::string &msg, bool override = false);

    /// Constant thread function return values
    const bool THREAD_SUCCESS = false;
    const bool THREAD_FAILURE = true;

    /// Internal status variables
    bool mStop;
    Status mStatus;

private:
    /// Boolean stopping status indicator
    /// \return - indication whether the thread is stopping
    [[nodiscard]] bool IsStopping() const;

    /// Boolean continuing status indicator
    /// \return - indication whether the thread is continuing
    [[nodiscard]] bool IsContinuing() const;

    /// STL void function definition
    typedef std::function<void(void)> VOID_FUNC;

    /// STL bool return function definition
    typedef std::function<bool(void)> BOOL_FUNC;

    /// Generic try catch functionality
    /// \param func - function to be run
    /// \param success - function to call if exception was not thrown
    /// \param fail - function to call if exception was thrown
    void TryCatchFunc(const BOOL_FUNC &func, const VOID_FUNC &success, const VOID_FUNC &fail, const VOID_FUNC &exception);

    /// Internal constant variables
    const int CATCH_MAX = 3;
    const bool DEBUG;

    /// Timer variables
    bool mDurationSet;
    std::chrono::microseconds mThreadDuration;
    Timer mThreadTimer;

    /// General internal variables
    bool mInitFail;
    int mLoopThrowCount;

    /// Thread data mutex
    std::mutex mDataLock;
};


#endif //DEV_MAIN_THREAD_H
