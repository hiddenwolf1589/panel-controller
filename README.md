# README #

### What is this repository for? ###

LED panel controller application

### How do I get set up? ###

1. Run the following commands from the linux command line to set up interface
    1. modprobe vcan
    1. ip link add dev <interface name> type vcan
        1. ip link add dev can0 type vcan
    1. ip link set up <interface name>
        1. ip link set up can0
1. Build using cmake
1. Run application from command line